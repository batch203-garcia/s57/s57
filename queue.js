let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    collection[collection.length] = item;
    return collection;
}

function dequeue(){
    let dequeuedCollection =[];
    for(let i=1; i < collection.length ; i++){
        dequeuedCollection[dequeuedCollection.length] = collection[i]
    }
    collection = dequeuedCollection;
    return collection;

}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    if(collection.length === 0 ){
        return true;
    }
    else{
        return false;
    }
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};